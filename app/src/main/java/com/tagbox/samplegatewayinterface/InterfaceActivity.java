package com.tagbox.samplegatewayinterface;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
//import com.tagbox.tag_sync.api.TagSyncInitializer;

import java.util.ArrayList;
import java.util.List;


import static com.tagbox.samplegatewayinterface.Constants.API_KEY;
import static com.tagbox.samplegatewayinterface.Constants.DEMO_MX_SCAN_DEVICE_MAC_ADDRESS;
import static com.tagbox.samplegatewayinterface.Constants.DEMO_SENSOR_CLIENT_ID;
import static com.tagbox.samplegatewayinterface.Constants.DEMO_THERMAL_GUN_MAC_ADDRESS;

import com.tagbox.tag_sync.api.TagSyncInitializer;
import com.tagbox.tag_sync.receiver.TemperatureDataRecordingCallback;
import com.tagbox.tag_sync.receiver.ThermalGunListener;

import org.json.JSONObject;


public class InterfaceActivity extends AppCompatActivity {

    private Button startButton;
    private Button fetchSensorDataButton;
    private Button connectThermalGunButton;
    private Button checkButton;

    private Button disconnectThermalGunButton;
    private Button startMxScanTemperatureRecording;
    private Button stopMxScanTemperatureRecording;
    private static final int REQUEST_ID_ALL_PERMISSIONS = 87;

    public ThermalGunListener thermalGunListener = new ThermalGunListener() {
        @Override
        public void onDeviceConnected(String deviceId, boolean success) {
            Log.d("thermalOnConnect",success+"");
        }
        @Override
        public void onTemperatureDataReceived(String deviceId,double temperature, long timestamp) {
            Log.d("thermalData",deviceId+" "+temperature+" "+timestamp);
        }

        @Override
        public void onDisconnect(String deviceId,boolean success) {
            Log.d("thermalOnDisconnect",deviceId+" "+success);
        }
    };

    public TemperatureDataRecordingCallback temperatureDataRecordingCallback = new TemperatureDataRecordingCallback() {

        @Override
        public void onTemperatureRecordingRegistered(String deviceId, boolean success) {
            Log.d("tempOnConnect",deviceId+" "+success);
        }

        @Override
        public void onTemperatureDataReceived(String deviceId,double temperature, long timestamp) {
           Log.d("mxTemp",deviceId+" "+temperature+" "+timestamp);
        }

        @Override
        public void onStop(String deviceId, boolean success) {
            Log.d("mxTempOnStop",deviceId+" "+success);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interface);

        startButton = (Button) findViewById(R.id.start_button);
        fetchSensorDataButton = (Button) findViewById(R.id.fetch_sensor_button);
        connectThermalGunButton = (Button) findViewById(R.id.connect_thermal_gun);
        disconnectThermalGunButton = (Button) findViewById(R.id.disconnect_thermal_gun);
        startMxScanTemperatureRecording = (Button) findViewById(R.id.connect_mxscan);
        stopMxScanTemperatureRecording = (Button) findViewById(R.id.disconnect_mxscan);
        checkButton = (Button) findViewById(R.id.check_button);



        //start TagSync scanner
        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkIfLocationPermissionMissing() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    // one time
                    launchRuntimeLocationPermission();
                }
                else if(!isLocationEnabled(getApplicationContext())){
                    // show a toast asking user to enable location

                    //or send user to location  settings to enable
                    Intent intent = new Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
                else {
                    // location permission is enabled

                    //location is turned on
                    TagSyncInitializer.startScanner(getApplicationContext(),API_KEY);
                }
            }
        });



        // fetching sensor data for a sensor
        fetchSensorDataButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                JSONObject jsonObject = TagSyncInitializer.fetchSensorData(getApplicationContext(),DEMO_SENSOR_CLIENT_ID);
                try {
                    Toast.makeText(getApplicationContext(),jsonObject+"",Toast.LENGTH_LONG).show();
                    //Log.d("sensor data",jsonObject.getString("utcTimestamp")+"");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        //fetching last known location of sensor
        connectThermalGunButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                TagSyncInitializer.connectThermalGunDevice(DEMO_THERMAL_GUN_MAC_ADDRESS,getApplicationContext(),thermalGunListener);
            }
        });

        disconnectThermalGunButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TagSyncInitializer.disconnectThermalGun(DEMO_THERMAL_GUN_MAC_ADDRESS,getApplicationContext(),thermalGunListener);

            }
        });


        startMxScanTemperatureRecording.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TagSyncInitializer.startTemperatureDataRecording(DEMO_MX_SCAN_DEVICE_MAC_ADDRESS,getApplicationContext(),temperatureDataRecordingCallback);
//
            }
        });

        stopMxScanTemperatureRecording.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TagSyncInitializer.stopTemperatureDataRecording(DEMO_MX_SCAN_DEVICE_MAC_ADDRESS,getApplicationContext(),temperatureDataRecordingCallback);
//
            }
        });



        checkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("is Scanner Up",TagSyncInitializer.isTagSyncServiceRunning(getApplicationContext())+"");
            }
        });

    }

    public boolean launchRuntimeLocationPermission(){
        List<String> listPermissionsNeeded = new ArrayList<>();

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                listPermissionsNeeded.add(Manifest.permission.BLUETOOTH_SCAN);
                listPermissionsNeeded.add(Manifest.permission.BLUETOOTH_CONNECT);
            }
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_ALL_PERMISSIONS);
            return true;
        }
        else{
            return false;
        }
    }

    public void openSettings(){
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        startActivity(intent);
    }

    public boolean checkIfLocationPermissionMissing(){

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
               return true;
        }
        else{
            return false;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_ALL_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                boolean checkPermission = true;

                for(int i=0;i<grantResults.length;i++){
                    checkPermission = checkPermission && (grantResults[i] == PackageManager.PERMISSION_GRANTED );
                }
                if (checkPermission) {
                    TagSyncInitializer.startScanner(getApplicationContext(),API_KEY);

                } else {
                    Toast.makeText(getApplicationContext(), "Location Permission was denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    public static Boolean isLocationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // This is a new method provided in API 28
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isLocationEnabled();
        } else {
            // This was deprecated in API 28
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            return (mode != Settings.Secure.LOCATION_MODE_OFF);
        }
    }

}
