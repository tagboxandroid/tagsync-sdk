package com.tagbox.samplegatewayinterface;

public final class Constants {
    
    public static final String DEMO_SENSOR_CLIENT_ID = "21000301865";
    public static final String DEMO_THERMAL_GUN_MAC_ADDRESS = "01:B6:EC:BA:94:9C";
    public static final String DEMO_MX_SCAN_DEVICE_MAC_ADDRESS = "F8:7D:BB:F9:D2:34";
    public static final String API_KEY = "9805fa7e5b56643e3af3eddabf62340";

}
