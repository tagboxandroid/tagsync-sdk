**TagSync Integration Documentation**


Release notes

Latest version - 1.9.2384909730



Release Notes 1.9.2384909730(latest) - Thermal gun data recording, scan temperature data recording


   1. For thermal gun data recording

     i. For registering thermal gun, call this method to connect with thermal gun and start temperature recording.
             
         TagSyncInitializer.connectThermalGunDevice(thermalGunMacAddress,getApplicationContext(),thermalGunListener);

        
     ii. Implement a listener in the activity or class in which the recording is to be listened.

          public ThermalGunListener thermalGunListener = new ThermalGunListener() {
                @Override
                public void onDeviceConnected(String deviceId, boolean success) {
                    Log.d("thermalOnConnect",success+"");
                }
                @Override
                public void onTemperatureDataReceived(String deviceId,double temperature, long timestamp) {
                    Log.d("thermalData",deviceId+" "+temperature+" "+timestamp);
                }
    
                @Override
                public void onDisconnect(String deviceId,boolean success) {
                    Log.d("thermalOnDisconnect",deviceId+" "+success);
                }
          };


      
     iii. Disconnect thermal gun after usage is completed
            
            TagSyncInitializer.disconnectThermalGun(currentAddress,getApplicationContext(),thermalGunListener);



  2. For MXScan temperature data recording
  
    i. For registering temperature recording, call this method to start MxScan temperature recording.
          
           TagSyncInitializer.startTemperatureDataRecording(currentAddress,getApplicationContext(),temperatureDataRecordingCallback);
        
     ii. Implement a listener in the activity or class in which the recording is to be listened.
           
           public TemperatureDataRecordingCallback temperatureDataRecordingCallback = new TemperatureDataRecordingCallback() {

                @Override
                public void onTemperatureRecordingRegistered(String deviceId, boolean success) {
                    Log.d("tempOnConnect",deviceId+" "+success);
                }
        
                @Override
                public void onTemperatureDataReceived(String deviceId,double temperature, long timestamp) {
                   Log.d("mxTemp",deviceId+" "+temperature+" "+timestamp);
                }
        
                @Override
                public void onStop(String deviceId, boolean success) {
                    Log.d("mxTempOnStop",deviceId+" "+success);
        
                }
           };

     
     iv. Stop MXScan temperature recording
            
                TagSyncInitializer.stopTemperatureDataRecording(currentAddress,getApplicationContext(),temperatureDataRecordingCallback);

            


********************

    Release Notes 1.9.2384909695 - Gradle plugin update 
        
        Supported Android gradle plugin version -> 5.4.1(min)  to  7.2.2(latest)
        Supported Gradle version -> 3.6.4(min) to  7.3.3(latest)

    
        1. In app level build gradle file include the following for latest gradle version support(refer sample app build gradle file):
 

            android {
                    packagingOptions {
                        exclude 'AndroidManifest.xml'
                        exclude 'resources.arsc'
                        exclude 'META-INF/com.android.tools/proguard/coroutines.pro'
                    }
            }
           
            2.  implementation 'co.tagbox:tagsync:1.9.2384909695'



********************
     

    Release notes 1.9.2384909

        1. Compatibility with google material library


********************


    Release Notes 1.9.238489(Major Update)

        1. Temeperature telemetry data upload related optimisations

********************

    Updates in version 1.9.238482(Patch)

        1. Bluetooth scanner issue on One plus phones when scanner runs for long periods

********************

    Updates in version 1.9.238481(Major release)

         1. Bug fix related to foreground service(Android 12) starting from background
         2. Pending intent(Android 12) related fixes

           Link - https://developer.android.com/about/versions/12/behavior-changes-12#foreground-service-launch-restrictions

           https://developer.android.com/about/versions/12/behavior-changes-12#pending-intent-mutability 

********************


    Updates in version 1.9.238467

         1. Scanner bug fixes 


********************


    Updates in version 1.9.238444(Important for ble behaviour in Android 12 phones)

          1. Android 12 related runtime permissions added in sample app for bluetooth scan and bluetooth connect

               Link - https://developer.android.com/guide/topics/connectivity/bluetooth/permissions

********************


    Updates in version 1.9.238436

          1. The timestamp of the data could be stale due to either bluetooth scanner not working due to system interruption or the sensor being away. In case, the timestamp is not getting updated the data that comes will be accompanied with an action code. Based on that, you can ask the user to toggle bluetooth switch which allows bluetooth adpater to reset and
              scanner to work again.

               {
                   "sensorId":"MX56BE",
                   "temperature":25.6, // temperature in celsius
                   "utcTimestamp": "2019-09-14 05:29:19", // timestamp in utc
                   "actionCode",206,
                   "message","Sensor data is relatively old. If sensor is nearby, try toggling bluetooth switch"
            
                }    


          2. It is recommended to turn off batttery optimization in order to make the ble scanner to work longer without getting interrupted by system.


********************


**Software requirements** :

Android Version: 5.0+

Apps:

Host app (communicates and interacts with BLE by calling TagSync methods)



**Getting Started:**

The application will have the following two methods that needs to be instantiated from the host application (described in Developer Guide):

    startScanner – for starting Bluetooth scanner and collecting temperature data
    
    fetchSensorData - getting the recent sensor data from TagSync along with timestamp
    
    fetchSensorlocationData - getting the last known location data of sensor long with timestamp

For the Bluetooth scanner to work properly, the location needs to be turned on from phone settings. Hence, whenever "Start Scanner" method is being called, the host application needs to check location status and prompt the user to turn it on in high accuracy mode.

   *This user flow has been implemented in the sample app illustrating runtime location flow and enabling location settings if required. The flow can be tweaked according to the user's use case.*

There can be different scenarios in which the app fails to get recent sensor data. The error codes can be following:

   i. sensor not being in the vicinity of the phone

   ii. location turned off
   
   iii. Bluetooth scanner on the android device is not working properly
   
   
**Developer Guide:**


*Using tag_sync dependency in your app:*

   1. In app level build.gradle declare following inside your repositiories section:
    
        repositories {
            maven {
                url 'https://pkgs.dev.azure.com/tagbox/Production/_packaging/sdks/maven/v1'
                credentials {
                    username "sdks"
                    password System.getenv("SYSTEM_ACCESSTOKEN") != null ? System.getenv("SYSTEM_ACCESSTOKEN") : "${properties.getProperty("tagboxSdkToken")}"
                }
            }
        }
    
   2. The 'tagboxSdkToken' variable allows access to private tagsync sdk repository.
      In the sample app, 'tagboxSdkToken' is picked from local.properties file. Create a local.properties file inside your app if it is not already there and include as follows:
      

      tagboxSdkToken=weghty2pwwzbmneul4tueli3a6qybjeiqpjkvqqplinvgjph5xq


   For accessing public SDK method, declare following inside dependency section
   
       implementation 'co.tagbox:tagsync:1.9.238412'


   For accessing public libraries included in sdk, include the 'jcenter' in project level gradle file as follows:


      allprojects {
         repositories {
         google()
         jcenter()
         flatDir {
            dirs 'libs'
            }
         }
      }

    
1. Calling startScan method from the host app:

    This method will invoke the scanner on the TagSync and start collecting the data from relevant sensor in background. After some time interval (typically a few minutes) this activity is stopped, shutting off the Bluetooth Scanner to optimize battery life. Hence, this method should be called before each fetchData call with some delay (recommended delay is 2mins) to ensure that telemetry data received from fetchData is not stale.
   
   
      TagSyncInitializer.startScanner(context,API_KEY);
    
    
2. Calling fetchSensorData method from the host app:

    Calling this method will return a Json that contains data or an error message that is illustrated in the following section.

    The sensor data in response to this method is in JSON format and has a data field and a timestamp field in unix epoch format. The application picks up the latest telemetry packet of the relevant sensor device at each call. To ensure that the date is not stale, startScan method should be called at least 2 minutes before an attempt to fetch data.

        JsonObject sensorData = TagSyncInitializer.fetchSensorData(context, sensorId);

  The data that comes corresponding to fetchSensorData method will have the data field in the following format:


    {
    "sensorId":"MX56BE",
    "temperature":25.6, // temperature in celsius
    "utcTimestamp": "2019-09-14 05:29:19" // timestamp in utc
    }
  

    
  The error data that comes will have the following format"

    { 
       "errorCode":301,
       "errorMessage":"Location is not enabled"
    }

3. Calling fetchLocationData method from the host app:

   The node location data is fetched in same way as fetching sensor temperature data. This value gives the last seen location of sensor.
        
       JsonObject locationData = TagSyncInitializer.fetchLocationData(context, sensorId);
    
   Following are the possible error codes:

        301 : location not enabled
        302 : bluetooth adapter not working
        303: sensor data not found
        306: Sensor mapping not found. Need network to fetch sensor mapping    
        308: Sensor not found in available mapping    
        307: Location data is not available. Check locations settings
    
The permission check message will have the following format"

       { 
       "permission_flag":{ 
              "location_permission":false,
               "storage_permission":true
          }
        }

4. Calling checkScannerService method from the host app which checks whether the scanner service is running:


         Boolean isScannerRunning = TagSyncInitializer.checkTagSyncScannerWorking(context);
         
   This method returns a boolean value.   
   
    
5. Calling checkLocationPermission method from the host app whether the app has location permission:

    
          Boolean hasLocationAccess = TagSyncInitializer.checkLocationPermission(context);
          
          
   This method returns a boolean value. 
   
